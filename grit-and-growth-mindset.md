# Grit and Growth Mindset

### 1. Paraphrase (summarize) the [video](https://www.youtube.com/watch?v=H14bBuluwB8) in a few (1 or 2) lines. Use your own words.

Dr. Angela Duckworth, a professor of psychology at Stanford University, discusses the concept of grit, which she defines as the ability to learn and achieve success despite setbacks.She also discusses that grit is the key to success, even more important than intelligence or talent.

### 2. Paraphrase (summarize) the [video](https://www.youtube.com/watch?v=75GFzikmRY0) in a few (1 or 2) lines in your own words.


It highlights that people with a growth mindset believe in their ability to learn and grow through effort, embracing challenges and setbacks as opportunities for improvement. Conversely, people with a fixed mindset believe their intelligence and talents are fixed, leading them to fear challenges and give up easily when facing obstacles. 

### 3. What is the Internal Locus of Control? What is the key point in the [video](https://www.youtube.com/watch?v=8ZhoeSaPF-k)?
Internal Locus of Control means you have control over your life and that you are responsible for the things that happen to you. The key point in the video is that having an internal locus of control is essential for staying motivated. The video also provides tips on how to develop an internal locus of control. The most important tip is to take responsibility for your own life and to believe that you have the power to change your circumstances. The video also suggests that it is helpful to solve problems in your own life and to take some time to appreciate the fact that it was your actions that solved the problem.

### 4.What are the key points mentioned by speaker to build growth mindset (explanation not needed).

- Believe in your ability to learn and grow
- Question your assumptions
- Develop your own life curriculum
- Honor the struggle

### 5. What are your ideas to take action and build Growth Mindset?

- Focus on effort and process, not outcome
- Set Realistic Goals
- Celebrate your progress
- Reflect Regularly


