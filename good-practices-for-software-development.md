# Good Practices for Software Develpoment

### 1. Which point(s) were new to you?

- Blocking social media sites and apps during work hours. Tools like TimeLimit, Freedom, or any other app can be helpful.
- Tracking your time using app like Boosted to improve productivity.

### 2. Which area do you think you need to improve on? What are your ideas to make progress in that area?

I should do some light exercise to improve productivity and also keeping my phone away during work.

I can start progressing in this area by keeping my phone on do not disturb while working and walking 5000 steps daily.

