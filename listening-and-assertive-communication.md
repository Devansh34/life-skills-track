# Listening and Assertive communication

### 1.What are the steps/strategies to do Active Listening? (Minimum 6 points).

The following steps to do Active Listening:

- Avoid getting distracted
- Focus on the speaker
- Don't interrupt
- Use door openers
- Show you are listening with body language
- Paraphrase

### 2.According to Fisher's model, what are the key points of Reflective Listening?

- Repeat what the other person said in your own words. 
- Express the same idea using different words.
- Let the person know that their thoughts and feelings are important to you, creating a sense of validation.
- Identify and acknowledge the emotions behind the words.

### 3.What are the obstacles in your listening process?

Having preexisting opinions or biases regarding the speaker or the subject.

### 4.What can you do to improve your listening?
Don't decide what you think too quickly. Wait until you really understand what the person is saying before making up your mind.

### 5.When do you switch to Passive communication style in your day to day life?

I switch to Passive communication style when  I want to avoid conflict.I prefer a harmonious environment and may choose a passive style to maintain peace in situations.

### 6.When do you switch into Aggressive communication styles in your day to day life?

I generally do not switch into Aggressive communication style in my day to day life.

### 7.When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

I generally do not wtich into passive Aggressive communication in my day to day life.

### 8.How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life?

Few Steps I can apply in my life:
- Clear and concise language
- Active listening
- Express Your Needs and Wants
- Stay Calm and Respectful
- Confident body language
