# Focus Management

### 1. What is Deep Work?

Deep work is a type of cognitively demanding task that is very beneficial, which they call hard focus.

### 2. According to author how to do deep work properly, in a few points?

- Limit your time on social media.
- Don't juggle multiple tasks; conquer them one by one.
- Break down large tasks 
- Schedule deep work 

### 3. How can you implement the principles in your day to day life?

- Find a quiet place
- Leverage technology to block distractions and enhance your deep work experience.
- Cultivating deep work habits takes time and effort. Be consistent and celebrate your progress along the way.


### 4. What are the dangers of social media, in brief?
- Addictive: Social media are designed to be addictive.
- Distractions: The constant notifications and updates can be highly distracting, making it difficult to focus on important tasks and achieve deep work. 
- Anxiety and Depression: Studies have linked excessive social media use to increased anxiety and depression, particularly in teenagers and young adults
- Privacy Concerns: Sharing personal information on social media can pose privacy risks. 
- Misinformation: Social media platforms can be breeding grounds for misinformation and fake news.
- Sleep Disruption: Late-night scrolling and the blue light emitted from screens can disrupt sleep patterns, leading to fatigue.
