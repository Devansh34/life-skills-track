# Prevention of Sexual Harassment

### What kinds of behaviour cause sexual harassment?

- **Unwanted Advances:** Any inappropriate physical or verbal approaches, including unwanted physical contact, sexually provocative remarks, or jokes about sex.

- **Offensive Comments or Humor:** Making crude remarks, jokes, or sexually suggestive remarks that make people feel uneasy.

- **Inappropriate Emails or Messages:** Sending goods, emails, or other communications over digital channels that are sexually explicit.

- **Sexual Discrimination:** Person being treated unfairly or negatively because of their gender; this might involve making disparaging remarks or acting in an unfair manner.

- **Retaliation for Reporting:** Retaliating against an individual who reports sexual harassment.


### What would you do in case you face or witness any incident or repeated incidents of such behaviour?

- **Speak Up:** If it is safe and comfortable for you to do so, speak with the individual directly and let them know that their behavior is wrong and unwanted.

- **Know Your Rights:** Understand your legal rights in your jurisdiction and become acquainted with your company's sexual harassment policies.

- **Use company reporting measure:** Follow your company's reporting procedures, which may include filing a formal complaint or, if available, using an anonymous reporting system.

- **Document the Incident:** Keep a detailed record of the incident, including dates, times, locations, individuals involved, and a description of the behavior


