# Learning Process

### 1. What is the Feynman Technique? Explain in 1 line.

Feynman Technique is a learning process which involves pretending to teach a child to identify the gap in your knowledge.


### 2. In this [video](https://www.youtube.com/watch?v=O96fE1E-rf8), what was the most interesting story or idea for you?
One of the key ideas in the video is that learning is not just about understanding. It is also about practice and repetition. Oakley recommends using the Pomodoro Technique, which involves working for 25 minutes and then taking a 5-minute break.


### 3.What are active and diffused modes of thinking?

Focused and Diffuse Thinking describes two counterposed methods of approaching problems and learning. Focused Thinking is concentrated, conscious and relatively predictable. By contrast, Diffuse Thinking is relaxed, occurs largely subconsciously, and can result in surprising connections.


### 4.According to the [video](https://www.youtube.com/watch?v=5MgBikgcWnY) , what are the steps to take when approaching a new topic? Only mention the points.

- Deconstruct the skill
- Learn Enough to self-correct
- Remove Practice Barrier
- Practice 20 hours


### 5.What are some of the actions you can take going forward to improve your learning process?

- Put 20 hours into anything you want to learn. It doesn't matter what you want to learn, whether it is a language, cooking, drawing, or anything else that turns you on or lights you up. We should just go out and do it.

- Learning is not just about focus. It is also about relaxation. Oakley recommends taking breaks to relax and let your mind wander. She also recommends getting exercise, which has been shown to improve learning and memory.

