# Energy Management

### 1. What are the activities you do that make you relax - Calm quadrant?

- Listening Music
- Walking

### 2. When do you find getting into the Stress quadrant?
- When I have confict or challenges with my closed ones.

### 3. How do you understand if you are in the Excitement quadrant?
- When I feel positivity around me. I think i am in excitement quadrant.
 
### 4. Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

- Sleep is essential for learning and memory.
- Sleep deprivation can lead to a 40% decrease in the ability to learn new information.
- Deep sleep is essential for memory consolidation.
- Sleep is also essential for physical health.
- Sleep loss can increase the risk of heart disease, cancer, and other health problems.
- We need to prioritize sleep and get enough of it each night.

### 5. What are some ideas that you can implement to sleep better?

- Limit screen time before bed.
- Avoid heavy meal before bedtime.
- Making a bedtime schedule.


### 6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

- Exercise has immediate effects on brain.
- One should exercise for 30 minutes 3-4 times a week.
- Improves blood flow
- Regular exercise helps in improving memory and protecting against neurodegenerative diseases
- Elevates mood


### 7. What are some steps you can take to exercise more?

- make it a habit.
- track my progress.
- make a schedule
- set realistic goals.
