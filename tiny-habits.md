# Tiny Habits

### 1. In this [video](https://www.youtube.com/watch?v=AdKUJxjn-R8), what was the most interesting story or idea for you?
The most interesting idea for me was  that BJ Fogg, the speaker, argues that we should focus on making small, easy changes to our behavior instead of trying to make big, drastic changes all at once. He believes that this is a more effective way to create lasting change.

### 2. How can you use B = MAP to make making new habits easier? What are M, A and P.
- B: Behavior - The new habit you want to form.
- M: Motivation - Your desire or reason for doing the behavior.
- A: Ability - Your capacity to do the behavior.
- P: Prompt - The trigger or reminder to do the behavior.


### 3. Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)

Celebrating or "shining" after each successful completion of a habit is important for several reasons:

- Increased Motivation and Commitment
- Enhanced Learning and Memory
- Counteracting Negativity


### 4. In this [video](https://www.youtube.com/watch?v=mNeXuCYiE0U), what was the most interesting story or idea for you?
 The most interesting story or idea for me was the story of Dave Brailsford and his philosophy of the aggregation of marginal gains. This philosophy is about making small, 1% improvements in everything you do, and Brailsford used it to turn British Cycling from a mediocre team into a powerhouse that won the Tour de France four times in five years.


### 5. What is the book's perspective about Identity?
Identity is the most important factor in changing habits. We should focus on becoming the type of person who has the habits we want, rather than simply trying to force ourselves to do the behaviors. For example, if we want to become a healthy person, we should focus on developing the identity of a healthy person. 

### 6. Write about the book's perspective on how to make a habit easier to do?
Systems are more sustainable and less likely to be derailed by setbacks. For example, instead of setting a goal to exercise for 30 minutes every day, you could create a system that makes it easy to exercise, such as putting your workout clothes out the night before or scheduling your workouts in your calendar.

### 7. Write about the book's perspective on how to make a habit harder to do?
- Make it invisible.
- Make it unattractive.
- Make it difficult.
- Make it unsatisfying.

### 8. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I would like to have healthy eating habits.The steps i would take is:

- Prepare your meals and snacks in advance 
- Incorporate my favorite healthy food
- Start small and gradually make changes
- Track my  progress

### 9. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

The habit i would like to eliminate is staying up late when I should be sleeping.
The steps:
- Create a relaxing bedtime routine
- Remind myself of the negative consequences of not getting enough sleep
- Turn off electronic devices at least an hour before bedtime
- Avoid rewarding yourself for staying up late
